//
//  TableViewModel.swift
//  Realm Demo
//
//  Created by iOS_Razrab on 13/10/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import Foundation
import RealmSwift

class TableViewModel {
    
    
    func queryPeople () -> [Human] { // === начало: управление элементами в БД
        
        var people = [Human]()
        
        let peopleObjects = RealmHelper.realm.objects(Human.self)
        
        for object in peopleObjects {
            people.append(object)
        }
        
        return people
        
    }// === конец: управление элементами в БД
    
    
}

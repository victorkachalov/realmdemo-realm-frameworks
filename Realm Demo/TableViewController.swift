//
//  TableViewController.swift
//  Realm Demo
//
//  Created by Victor Kachalov on 13.09.17.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import RealmSwift

class TableViewController: UITableViewController {

    
    var people = [Human]()
    
    let tableViewModel = TableViewModel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        people = tableViewModel.queryPeople()
        
        
        //let predicate = NSPredicate(format: "name BEGINSWITH [S]%@", people)
        
        for person in people {
        
            let predicate = NSPredicate(format: "name BEGINSWITH [s]%@", person.name)
            
            print("Сказуемое: ", predicate)
            
        }
        
//        people = tableViewModel.queryPeople().filter({ (person) -> Bool in
//            person.age < 22
//        })
        
//        people = tableViewModel.queryPeople().sorted(by: { $1.name > $0.name })
        
     
        
//        let allPeople = RealmHelper.realm.objects(Human.self)
//        print("allPeople:", allPeople[0].name)
        
//         ===   можем сделать выборку
//                var adults = allPeople.filter("age > 21")
//                var byName = allPeople.sorted(byKeyPath: "name", ascending: true)
        
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        DispatchQueue.main.async() {
            self.tableView.reloadData()
            
        }
    }

    


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        let person = people[indexPath.row]
        
        cell.personName.text = person.name
        cell.personRace.text = person.race
        cell.personAge.text  = String(person.age)
        
        return cell
    }
    
 
    

}

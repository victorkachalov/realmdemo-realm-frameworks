//
//  ViewController.swift
//  Realm Demo
//
//  Created by iOS_Razrab on 13/10/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController {

    
    let mainViewModel = MainViewModel()
    
    @IBOutlet weak var inputName: UITextField!
    
    @IBOutlet weak var inputAge: UITextField!
    
    @IBOutlet weak var inputRace: UITextField!
    
    
    @IBAction func addPersontToDB(_ sender: UIButton) {
        
        let person = Human()
        
        person.name = inputName.text!
        person.age = Int(inputAge.text!)!
        person.race = inputRace.text!
        
        mainViewModel.addHuman(person: person)
        
        inputName.text = ""
        inputAge.text = ""
        inputRace.text = ""
        
    }
    
}

//
//  UserOptions.swift
//  Realm Demo
//
//  Created by iOS_Razrab on 11/10/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import Foundation
import RealmSwift


class UserOptions: Object {
    
    var firebaseLogin: String = ""
    var firebasePass : String = ""
    var userName     : String = ""
    
}

//
//  ObserveViewController.swift
//  Realm Demo
//
//  Created by iOS_Razrab on 13/10/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import RealmSwift

class ObserveViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    
    @IBOutlet weak var searchBar: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    let tableViewModel = TableViewModel()
    
    var people = [Human]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        people = tableViewModel.queryPeople()
        
        // Do any additional setup after loading the view.
    }


    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
  

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        let person = people[indexPath.row]
        
        cell.personName.text = person.name
        cell.personAge.text  = String(person.age)
        cell.personRace.text = person.race
        
        return cell
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        people.removeAll()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { 
            if (textField.text?.characters.count)! > 0 {
                
//                let predicate = NSPredicate(format: "name CONTAINS [c] %@", textField.text!)
                let predicate = NSPredicate(format: "name BEGINSWITH [c] %@", textField.text!)
                let filteredPeople = RealmHelper.realm.objects(Human.self).filter(predicate).sorted(by: { $1.name > $0.name })
                
                for each in filteredPeople {
                    self.people.append(each)
                }
                self.tableView.reloadData()
                
            } else {
                
                self.people = self.tableViewModel.queryPeople()
                
            }
        }
        
        return true
        
    }
    
}

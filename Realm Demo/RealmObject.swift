//
//  RealmObject.swift
//  Realm Demo
//
//  Created by Victor Kachalov on 13.09.17.
//  Copyright © 2017 vk. All rights reserved.
//

import Foundation
import RealmSwift


class Human : Object {
    
    dynamic var name = ""
    dynamic var age  = 0
    dynamic var race = ""
    
}

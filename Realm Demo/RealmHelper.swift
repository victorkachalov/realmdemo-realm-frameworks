//
//  RealmHelper.swift
//  Realm Demo
//
//  Created by iOS_Razrab on 13/10/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import Foundation
import RealmSwift

class RealmHelper {
    
    
    static private let realmSchemaVerion : UInt64 = 9
    
    static private let config: Realm.Configuration = {
        let config = Realm.Configuration(schemaVersion: realmSchemaVerion, migrationBlock: { migration, oldSchemaVersion in })
        print(config)
        return config
    }()


    static let realm = try! Realm(configuration: config)
    
}

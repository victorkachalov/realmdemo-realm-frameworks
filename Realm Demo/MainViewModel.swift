//
//  MainViewModel.swift
//  Realm Demo
//
//  Created by iOS_Razrab on 13/10/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import Foundation
import RealmSwift

class MainViewModel {
    

    let realmSchemaVerion : UInt64 = 9

    
    func addHuman(person: Human) { // === начало: сохраняем объекты в Realm (пример Human)
        
        try! RealmHelper.realm.write {
                RealmHelper.realm.add(person)
                print("added \(person) to Realm")
        }
        
    } // === конец: сохраняем объекты в Realm

    
    
    
}
